namespace SimpleBanking

module Auditing =
  let serializeTransaction transaction =
    sprintf "%O***%s***%M***%b"
      transaction.Timestamp
      transaction.Operation
      transaction.Amount
      transaction.Accepted

  let audit unwrapAccount writeTransaction (account:RatedAccount) (transaction:Transaction) = 
    let account = unwrapAccount account
    writeTransaction account.Owner.Name account.AccountId transaction

  let auditWithdrawAs (operationName:string) 
                      (audit:RatedAccount -> Transaction -> unit) 
                      (operation:decimal -> CreditAccount -> RatedAccount) 
                      (amount:decimal) 
                      (account:CreditAccount) : RatedAccount =
      let newAccount = operation amount account
      let transaction = { Timestamp = System.DateTime.UtcNow; Operation = operationName; Amount = amount; Accepted = true }
      audit newAccount transaction
      newAccount

  let auditDepositAs (operationName:string) 
                     (audit:RatedAccount -> Transaction -> unit) 
                     (operation:decimal -> RatedAccount -> RatedAccount) 
                     (amount:decimal) 
                     (account:RatedAccount) : RatedAccount =
      let newAccount = operation amount account
      let transaction = { Timestamp = System.DateTime.UtcNow; Operation = operationName; Amount = amount; Accepted = true }
      audit newAccount transaction
      newAccount