#load "SimpleBanking.fs"
#load "Domain.fs"
#load "Auditing.fs"
#load "Operations.fs"
#load "FileRepository.fs"

open SimpleBanking
open Auditing
open Operations
open FileRepository

type Console = System.Console

let folderPath = @"SimpleBanking\tmp\accounts"
let printCurrentBalance(account:RatedAccount) =
    let balance = 
        match account with
        | InCredit (CreditAccount account) -> account.Balance
        | Overdrawn account -> account.Balance
    Console.WriteLine (sprintf "Current balance is $%f" balance)
let writeTransaction' = 
    FileRepository.writeTransaction serializeTransaction folderPath
let consoleTransaction name accountId transaction = 
    Console.WriteLine (sprintf "Account %s: %s of %f (approved: %b)" name transaction.Operation transaction.Amount transaction.Accepted)
let auditOps operations name accountId transaction =
    operations
    |> Seq.iter(fun op -> op name accountId transaction)
let auditOps' = auditOps [writeTransaction'; consoleTransaction]
let audit' account transaction = 
    audit unwrapAccount auditOps' account transaction 
    printCurrentBalance account
let depositWithAudit = auditDepositAs "deposit" audit' deposit
let withdrawWithAudit = auditWithdrawAs "withdraw" audit' withdraw
let processCommand' = processCommand deposit withdraw
let processCommandWithAudit = processCommand depositWithAudit withdrawWithAudit
let runTransactions' = runTransactions getBankOperationWithAmount processCommand'
let loadAccount' tuple = loadAccount runTransactions' tuple
let tryLoadAccountFromDisk' = tryLoadAccountFromDisk (tryFindTransactionsOnDisk folderPath) loadAccount'
let account = openingAccount tryLoadAccountFromDisk' newAccount

printCurrentBalance account
let commands = seq { 
    while true do
        Console.Write "(d)eposit, (w)ithdraw or e(x)it: "
        yield Console.ReadKey().KeyChar }

let processCommands' account commands = processCommands processCommandWithAudit account commands
processCommands' account commands

// TODO: DB
// TODO: Suave
// TODO: Testing

#r @"..\packages\FSharp.Data\lib\net45\FSharp.Data.dll"
open FSharp.Data
type Football = CsvProvider< @"data\FootballResults.csv" >
let data = Football.GetSample().Rows |> Seq.toArray
data
|> Seq.filter(fun row ->
    row.``Full Time Home Goals`` > row.``Full Time Away Goals``)
|> Seq.countBy(fun row -> row.``Home Team``)
|> Seq.sortByDescending snd
|> Seq.take 10

type TvListing = JsonProvider<"http://www.bbc.co.uk/programmes/genres/comedy/schedules/upcoming.json">
let tvListing = TvListing.GetSample()
let title = tvListing.Broadcasts.[0].Programme.DisplayTitles.Title

#r @"..\packages\SQLProvider\lib\net451\FSharp.Data.SqlProvider.dll"

open FSharp.Data.Sql
let [<Literal>] dbVendor = Common.DatabaseProviderTypes.POSTGRESQL
let [<Literal>] indivAmount = 1000
let [<Literal>] useOptTypes  = true
let [<Literal>] owner = "public, admin, references"
let [<Literal>] connString = "Host=localhost;Port=5433;Database=FSharpTest;Username=postgres;Password=postgres"
let [<Literal>] resPath = @"c:\Users\he9li\Documents\GitHub\SimpleBanking\packages\Npgsql\lib\net45"

type Sql = SqlDataProvider<
                ConnectionString = connString,
                ResolutionPath = resPath,
                DatabaseVendor = Common.DatabaseProviderTypes.POSTGRESQL,
                CaseSensitivityChange = Common.CaseSensitivityChange.ORIGINAL>

let ctx = Sql.GetDataContext()
let accounts = ctx.Public.Accounts

let row = accounts.Create()
row.Accountid <- System.Guid.NewGuid()
row.Owner <- "Amy"
ctx.SubmitUpdates()

// 1. Add SqlRepository to handle insert and tryFind transactions
// 2. Add tests with different environment using different db (F# testing with db)
let accountsArray = ctx.Public.Accounts |> Seq.toArray
let acc = accountsArray.[0]
acc.Accountid
