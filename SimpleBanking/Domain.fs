namespace SimpleBanking

[<AutoOpen>]
module Domain =
    type Customer = { 
        Name: string 
    }

    type Account = { 
        AccountId: System.Guid
        Owner: Customer
        Balance: decimal
    }

    type Transaction = {
        Timestamp: System.DateTime
        Operation: string
        Amount: decimal
        Accepted: bool
    }

    type BankOperation = Deposit | Withdraw
    type Command = AccountCommand of BankOperation | Exit

    type CreditAccount = CreditAccount of Account
    type RatedAccount = 
        | InCredit of CreditAccount
        | Overdrawn of Account
