open System.Collections.Generic

type FootballResult =
    { HomeTeam : string
      AwayTeam : string
      HomeGoals : int
      AwayGoals : int }

type TeamSummary = { Name: string; mutable AwaysWins : int }

let create (ht, hg) (at, ag) =
    { HomeTeam = ht; AwayTeam = at; HomeGoals = hg; AwayGoals = ag }

let results =
    [ create ("Messiville", 1) ("Ronaldo City", 2)
      create ("Messiville", 1) ("Bale Town", 3)
      create ("Bale Town", 3) ("Ronaldo City", 1)
      create ("Bale Town", 2) ("Messiville", 1) 
      create ("Ronaldo City", 4) ("Messiville", 2) 
      create ("Ronaldo City", 1) ("Bale Town", 2) ]

let isAwayWin result = result.AwayGoals > result.HomeGoals

results
|> List.filter isAwayWin
|> List.countBy(fun result -> result.AwayTeam)
|> List.sortByDescending(fun (_, awayWins) -> awayWins)

let numbersArray = [| 1; 2; 3; 4; 6 |]
let firstNumber = numbersArray.[0]
let firstThreeNumbers = numbersArray.[0 .. 2]

let numbers = [ 1; 2; 3; 4; 5; 6]
let numbersQuick = [ 1..6 ]
let head :: tail = numbers
let moreNumbers = 0 :: numbers
let evenMoreNumbers = moreNumbers @ [ 7 .. 9]

// collect - flat map
type Order = { OrderId : int }
type Customer = { CustomerId : int; Orders : Order list; Town : string }
let customers : Customer list = []
let orders : Order list = customers |> List.collect(func c -> c.Orders)

let inventory = Dictionary<string, float>()
// let inventory = Dictionary<_, _>() works too!
inventory.Add("Apples", 0.33)
inventory.Add("Oranges", 0.23)
inventory.Add("Bananas", 0.45)
inventory.Remove "Oranges"
inventory.["Bananas"]
inventory.["Oranges"]

// immutable IDictionary
let inventory : IDictionary<string, float> =
    [ "Apples", 0.33; "Oranges", 0.23; "Bananas", 0.45 ]
    |> dict
inventory.Add("Pineapples", 0.85)
inventory.Remove "Bananas"

let inventory = 
    [ "Apples", 0.33; "Oranges", 0.23; "Bananas", 0.45 ]
    |> Map.ofList

let apples = inventory.["Apples"]
let pineapples = inventory.["Pineapples"]

let newInventory =
    inventory
    |> Map.add "Pineapples" 0.87
    |> Map.remove "Apples"

let cheapFruit, expensiveFruit = 
    inventory
    |> Map.partition(fun fruit cost -> cost < 0.3)

System.IO.Directory.EnumerateDirectories(".")
|> Seq.map System.IO.DirectoryInfo
|> Seq.map(fun info -> (info.Name, info.CreationTimeUtc))
|> Map.ofSeq

let myBasket = [ "Apples"; "Apples"; "Apples"; "Bananas"; "Pineapples" ]
let fruitsILike = myBasket |> Set.ofList
