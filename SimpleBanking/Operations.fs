namespace SimpleBanking
open System

module Operations =
    let classifyAccount account =
        if account.Balance >= 0M then (InCredit(CreditAccount account))
        else Overdrawn account

    let withdraw amount (CreditAccount account) =
        { account with Balance = account.Balance - amount }
        |> classifyAccount

    let unwrapAccount account = 
        match account with
        | InCredit (CreditAccount account) -> account
        | Overdrawn account -> account

    let deposit amount account =
        let account = unwrapAccount account
        { account with Balance = account.Balance + amount }
        |> classifyAccount

    let tryLoadAccountFromDisk tryFindTransactionsOnDisk loadAccount =
        let loadAccountOptional = Option.map loadAccount
        tryFindTransactionsOnDisk >> loadAccountOptional

    let openingAccount tryLoadAccountFromDisk newAccount : RatedAccount =
        Console.Write "Please enter your name: "
        let owner = Console.ReadLine()
        match (tryLoadAccountFromDisk owner) with
        | Some account -> account
        | None -> newAccount(owner)

    let newAccount name : RatedAccount = 
        { Owner = { Name = name }; AccountId = System.Guid.NewGuid(); Balance = 0M }
        |> classifyAccount

    let loadAccount processTransactions (name:string, accountId:Guid, transactions:seq<Transaction>) : RatedAccount =
        let account = 
            { Owner = { Name = name }; AccountId = accountId; Balance = 0M }
            |> classifyAccount
        processTransactions account transactions

    let isStopCommand command =
        match command with
        | Exit -> true
        | _ -> false

    let processCommand deposit withdraw (account:RatedAccount) (command:BankOperation, amount:decimal) = 
        match command with
        | Withdraw -> 
            match account with
            | InCredit account ->
                withdraw amount account 
            | Overdrawn _ as account -> 
                account
        | Deposit -> deposit amount account 


    let tryParseCommand char: Command option =
        match char with
        | 'w' -> Some (AccountCommand Withdraw)
        | 'd' -> Some (AccountCommand Deposit)
        | 'x' -> Some Exit
        |   _ -> None

    let tryGetBankOperation cmd =
        match cmd with
        | Exit -> None
        | AccountCommand op -> Some op

    let tryGetAmount (bankOperation:BankOperation) =
        Console.WriteLine()
        Console.Write "Enter Amount: "
        let amount = Console.ReadLine() |> Decimal.TryParse
        match amount with
        | true, amount -> Some(bankOperation, amount)
        | false, _ -> None

    let processCommands processCommand account commands =
        commands 
        |> Seq.choose tryParseCommand
        |> Seq.takeWhile (not << isStopCommand)
        |> Seq.choose tryGetBankOperation
        |> Seq.choose tryGetAmount
        |> Seq.fold processCommand account

    let getBankOperationWithAmount (transaction:Transaction) = 
        match transaction with
        | { Operation = "deposit"; Amount = amount } -> Some(Deposit, amount)
        | { Operation = "withdraw"; Amount = amount } -> Some(Withdraw, amount)
        | _ -> None

    let runTransactions getBankOperationWithAmount processCommand account transactions =
        transactions
        |> Seq.choose getBankOperationWithAmount
        |> Seq.fold processCommand account
