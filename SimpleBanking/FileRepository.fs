namespace SimpleBanking
open System.IO
open System.Text.RegularExpressions 

module FileRepository = 
  let private logFilePath dir name accountId = sprintf "%s/%s--%A.log" dir name accountId

  let private parseAccountId name (filePath:string) =
    let fileName = Path.GetFileName(filePath)

    let logFilePattern = @"(\w+)--(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})\.log"

    let (|Equals|_|) arg x = 
        if (arg = x) then Some() else None

    let result = Regex.Match(fileName, logFilePattern)

    match result.Groups.[1].Value with
    | Equals name -> 
        // TODO: move dependency
        match System.Guid.TryParse(result.Groups.[2].Value) with
        | (true, accountId) -> Some (filePath, accountId)
        | _ -> None
    | _ -> None

  let private findAccountIdFromFolder folderPath name =
    Directory.GetFiles(folderPath, "*.log")
    |> Array.tryPick (parseAccountId name) 

  let writeTransaction (serialize:Transaction -> string) (dir:string) (name:string) (accountId:System.Guid) (transaction:Transaction) =
    let logFile = logFilePath dir name accountId
    use wr = new StreamWriter(logFile, true)
    transaction
    |> serialize
    |> wr.WriteLine

  let private parseTransaction (line:string) : Transaction option =
    let pattern = @"(\d+\/\d+\/\d{4}\s\d+\:\d+\:\d+\s\w+)\*\*\*(\w+)\*\*\*(\d+)\*\*\*(\w+)"
    let result = Regex.Match(line, pattern)
    let operation = result.Groups.[2].Value

    // TODO: use monad
    match System.DateTime.TryParse(result.Groups.[1].Value) with
    | true, time -> 
        match System.Decimal.TryParse(result.Groups.[3].Value) with
        | true, amount -> 
            match System.Boolean.TryParse(result.Groups.[4].Value) with
            | true, accepted ->
              Some { Timestamp = time; Operation = operation; Amount = amount; Accepted = accepted }
            | _ -> None
        | _ -> None
    | _ -> None

  let tryFindTransactionsOnDisk (dir:string) (name:string) : (string * System.Guid * seq<Transaction>) option =
    match findAccountIdFromFolder dir name with
    | Some (filePath, accountId) -> 
        let transactions = 
          filePath
          |> File.ReadLines
          |> Seq.choose parseTransaction 
        Some (name, accountId, transactions)
    | None -> None
